package br.com.kafka.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * 
 * @author vanessaraujs@gmail.com
 *
 */

@Service
public class KafkaProducer {

	private static final String TOPIC = "kafka_topic";

	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	public void writeMessage(String message) {
		this.kafkaTemplate.send(TOPIC, message);
	}
}
