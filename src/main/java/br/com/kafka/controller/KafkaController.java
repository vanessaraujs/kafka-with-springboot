package br.com.kafka.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.kafka.producer.KafkaProducer;

/**
 * 
 * @author vanessaraujs@gmail.com
 *
 */

@RestController
public class KafkaController {

	private final br.com.kafka.producer.KafkaProducer producer;

	public KafkaController(KafkaProducer producer) {
		this.producer = producer;
	}

	@PostMapping("/publish")
	public void writeMessageToTopic(@RequestParam("message") String message) {
		this.producer.writeMessage("Message send producer: " + message);
	}

}
