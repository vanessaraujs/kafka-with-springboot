package br.com.kafka.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * 
 * @author vanessaraujs@gmail.com
 *
 */

@Service
public class KafkaConsumer {

	@KafkaListener(topics = "kafka_topic", groupId = "kafka_groupId")
	public void getMesage(String message) {
		System.out.println(message);
	}
}
